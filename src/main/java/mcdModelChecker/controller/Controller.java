package mcdModelChecker.controller;

import mcdModelChecker.McdModelChecker;
import mcdModelChecker._options.Options;
import mcdModelChecker.controller.types.content.Content;
import mcdModelChecker.controller.types.graph.GraphType;
import mcdModelChecker.controller.types.input.Selections;
import mcdModelChecker.model.Model;
import mcdModelChecker.utils.ListHelper;
import mcdModelChecker.view.View;
import mcdModelChecker.controller.exceptions.McdException;
import mcdModelChecker.view.types.AnalyzerState;

import java.util.Arrays;
import java.util.Vector;

import static mcdModelChecker.view.types.AnalyzerState.DEFAULT_STATE;

public class Controller {

    Model model;
    View view;
    Options options;
    ListHelper listHelper;

    public Controller(Model model, View view) {

        this.model = model;
        this.view = view;
        this.options = new Options();
        this.listHelper = new ListHelper();

        // default list selections
        String file = options.getXmlFileOrder()[0];
        GraphType graph = GraphType.values()[0];
        Integer step = null;
        String modelStr = options.getModels1Var()[0]; // change this to getModels2Var if you change selectedFile to a file with 2 properties
        Integer loops = 0;
        AnalyzerState state = DEFAULT_STATE;
        Selections defaultSelections = new Selections(file, graph, step, modelStr, loops, state);

        // how to get the graphs here?

    }

    public void initialRender(View view) {
        view.initialRender();
    }

    // controller utils

    /**
     * Small one liner that gives a string array of all enum members
     * Code found at <a href="https://stackoverflow.com/q/45507197">https://stackoverflow.com/q/45507197</a>,
     * accessed 9/17/20
     *
     * @return a string array all the constants in the enum {@link GraphType}.
     */
    public String[] graphTypes() {
        // enum to string array code from https://stackoverflow.com/q/45507197
        return Arrays.stream(GraphType.values()).map(Enum::toString).toArray(String[]::new);
    }

    /**
     * One liner that gets the current line number for an exception message
     * For use in {@link McdException}
     * Code found at <a href="https://stackoverflow.com/a/115027">https://stackoverflow.com/a/115027</a>,
     * accessed around 7/1/20
     *
     * @return an int of line number
     */
    public static int getLineNumber() {
        return Thread.currentThread().getStackTrace()[2].getLineNumber();
    }

}
