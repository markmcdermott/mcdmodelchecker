package mcdModelChecker.controller.types.content;

import java.util.List;

public class ListsData {

    List<String> files;
    String[] graphs;
    String[] steps;
    String[] models;
    String loops;
    String[] states;
    String[] labels;

}
