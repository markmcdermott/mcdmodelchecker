package mcdModelChecker;

import mcdModelChecker._options.Options;
import mcdModelChecker.controller.Controller;
import mcdModelChecker.model.Model;
import mcdModelChecker.utils.ListHelper;
import mcdModelChecker.view.View;

import javax.swing.*;

/**
 * Base superclass of the app, used to instantiate {@link Options} and {@link ListHelper} just one time each for the whole app.
 */
public class McdModelChecker {

    public Model model;
    public View view;
    public Options options;
    public ListHelper listHelper;

    public McdModelChecker() {

        this.model = new Model();
        this.view = new View();

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    new Controller(model, view);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

}
