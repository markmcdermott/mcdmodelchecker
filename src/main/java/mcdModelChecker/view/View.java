package mcdModelChecker.view;

import mcdModelChecker.McdModelChecker;
import mcdModelChecker.view.pages.Analyzer;

import javax.swing.*;

import java.awt.*;

public class View {

    // the only global variable components are those adding
    // components from another method and components used by listeners
    public JFrame frame;

    public void initialRender() {
        new Analyzer().renderAnalyzer();
    }

    void initFrame() {
        frame = new JFrame();
        // frame.setPreferredSize(new Dimension(options.getWindowWidth(), options.getWindowHeight()));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
    }

    public void render() {
        frame.validate();
        frame.pack();
        frame.setVisible(true);
    }


}
