import mcdModelChecker.controller.Controller;
import mcdModelChecker.model.Model;
import mcdModelChecker.view.View;

import javax.swing.*;

/**
 * @author      Mark McDermott {@literal mark@markmcdermott.io}
 * @version     0.2
 */
public class Main {

    /**
     * Java Swing base MVC code from https://www.tutorialspoint.com/explain-the-architecture-of-java-swing-in-java, accessed 9/17/20
     *
     * @param args generic blank string args for command line execution - nothing expected here for this app.
     */

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Model model = new Model();
                View view = new View();
                try {
                    new Controller(model, view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

}
